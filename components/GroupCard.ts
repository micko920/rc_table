// Styles
import './GroupCard.sass'

// Extensions
import CardComponent from './CardComponent'

// Components
import VLabel from 'vuetify/src/components/VLabel'

// Mixins
import Loadable from 'vuetify/src/mixins/loadable'

// Directives
import ripple from 'vuetify/src/directives/ripple'

// Utilities
import { convertToUnit, keyCodes } from 'vuetify/src/util/helpers'
import { breaking, consoleWarn } from 'vuetify/src/util/console'

// Types
import mixins from 'vuetify/src/util/mixins'
import { VNode } from 'vue/types'

const baseMixins = mixins(CardComponent, Loadable)
interface options extends InstanceType<typeof baseMixins> {
  $refs: {
    label: HTMLElement
    'prepend-inner': HTMLElement
    prefix: HTMLElement
    suffix: HTMLElement
  }
}

/* @vue/component */
export default baseMixins.extend<options>().extend({
  name: 'group-card',

  directives: { ripple },

  inheritAttrs: false,

  props: {
    appendOuterIcon: String,
    filled: Boolean,
    flat: Boolean,
    fullWidth: Boolean,
    label: String,
    outlined: Boolean,
    prefix: String,
    prependInnerIcon: String,
    reverse: Boolean,
    rounded: Boolean,
    shaped: Boolean,
    singleLine: Boolean,
    solo: Boolean,
    soloInverted: Boolean,
    suffix: String,
    underlined: Boolean,
    type: {
      type: String,
      default: 'text'
    }
  },

  data: () => ({
    labelWidth: 0,
    prefixWidth: 0,
    prependWidth: 0,
    isBooted: false
  }),

  computed: {
    classes(): object {
      return {
        'group-card': true,
        'group-card--full-width': this.fullWidth,
        'group-card--prefix': this.prefix,
        'group-card--single-line': this.isSingle,
        'group-card--solo': this.isSolo,
        'group-card--solo-inverted': this.soloInverted,
        'group-card--solo-flat': this.flat,
        'group-card--filled': this.filled,
        'group-card--is-booted': this.isBooted,
        'group-card--enclosed': this.isEnclosed,
        'group-card--reverse': this.reverse,
        'group-card--outlined': this.outlined,
        'group-card--rounded': this.rounded,
        'group-card--shaped': this.shaped,
        'group-card--underlined': this.underlined,
        ...this.themeClasses,
        ...this.elevationClasses
      }
    },
    isEnclosed(): boolean {
      return this.filled || this.isSolo || this.outlined || this.fullWidth
    },
    isSingle(): boolean {
      return this.isSolo || this.singleLine || this.fullWidth
    },
    isSolo(): boolean {
      return this.solo || this.soloInverted
    },
    labelPosition(): Record<'left' | 'right', string | number | undefined> {
      let offset = this.prefix && !this.labelValue ? this.prefixWidth : 0

      if (this.labelValue && this.prependWidth) offset -= this.prependWidth

      return this.$vuetify.rtl === this.reverse
        ? {
            left: offset,
            right: 'auto'
          }
        : {
            left: 'auto',
            right: offset
          }
    },
    showLabel(): boolean {
      return this.hasLabel && !this.isSingle
    },
    styles(): object {
      return this.measurableStyles
    },
    labelValue(): boolean {
      return !this.isSingle
    }
  },

  watch: {
    labelValue: 'setLabelWidth',
    outlined: 'setLabelWidth',
    label() {
      this.$nextTick(this.setLabelWidth)
    },
    prefix() {
      this.$nextTick(this.setPrefixWidth)
    }
  },

  created() {
    if (this.$attrs.hasOwnProperty('box')) {
      breaking('box', 'filled', this)
    }

    if (this.$attrs.hasOwnProperty('browser-autocomplete')) {
      breaking('browser-autocomplete', 'autocomplete', this)
    }

    if (this.shaped && !(this.filled || this.outlined || this.isSolo)) {
      consoleWarn('shaped should be used with either filled or outlined', this)
    }
  },

  mounted() {
    this.setLabelWidth()
    this.setPrefixWidth()
    this.setPrependWidth()
    requestAnimationFrame(() => (this.isBooted = true))
  },

  methods: {
    /** @public */
    blur(e?: Event) {
      this.onBlur(e)
    },
    genAppendSlot() {
      const slot = []

      if (this.$slots['append-outer']) {
        slot.push(this.$slots['append-outer'] as VNode[])
      } else if (this.appendOuterIcon) {
        slot.push(this.genIcon('appendOuter'))
      }

      return this.genSlot('append', 'outer', slot)
    },
    genPrependInnerSlot() {
      const slot = []

      if (this.$slots['prepend-inner']) {
        slot.push(this.$slots['prepend-inner'] as VNode[])
      } else if (this.prependInnerIcon) {
        slot.push(this.genIcon('prependInner'))
      }

      return this.genSlot('prepend', 'inner', slot)
    },
    genContentSlot() {
      const content = CardComponent.options.methods.genContentSlot.call(this)

      const prepend = this.genPrependInnerSlot()

      if (prepend) {
        content.children = content.children || []
        content.children.unshift(prepend)
      }

      return content
    },
    genIconSlot() {
      const slot = []

      if (this.$slots['append']) {
        slot.push(this.$slots['append'] as VNode[])
      } else if (this.appendIcon) {
        slot.push(this.genIcon('append'))
      }

      return this.genSlot('append', 'inner', slot)
    },
    genDefaultSlot() {
      return [
        this.genFieldset(),
        this.genTextFieldSlot(),
        this.genIconSlot(),
        this.genProgress()
      ]
    },
    genFieldset() {
      if (!this.outlined) return null

      return this.$createElement(
        'fieldset',
        {
          attrs: {
            'aria-hidden': true
          }
        },
        [this.genLegend()]
      )
    },
    genLabel() {
      if (!this.showLabel) return null

      const data = {
        props: {
          absolute: true,
          dark: this.dark,
          disabled: this.disabled,
          for: this.computedId,
          left: this.labelPosition.left,
          light: this.light,
          right: this.labelPosition.right,
          value: this.labelValue
        }
      }

      return this.$createElement(VLabel, data, this.$slots.label || this.label)
    },
    genLegend() {
      const width =
        !this.singleLine && (this.labelValue || this.isDirty)
          ? this.labelWidth
          : 0
      const span = this.$createElement('span', {
        domProps: { innerHTML: '&#8203;' }
      })

      return this.$createElement(
        'legend',
        {
          style: {
            width: !this.isSingle ? convertToUnit(width) : undefined
          }
        },
        [span]
      )
    },
    genChildrenContent() {
      const listeners = Object.assign({}, this.$listeners)

      return this.$createElement(
        'div',
        {
          staticClass: 'group-card__content',
          style: {},
          attrs: {
            ...this.$attrs,
            disabled: this.disabled,
            id: this.computedId,
            readonly: this.readonly,
            type: this.type
          },
          ref: 'content'
        },
        [this.$slots.default]
      )
    },
    genTextFieldSlot() {
      return this.$createElement(
        'div',
        {
          staticClass: 'group-card__slot'
        },
        [
          this.genLabel(),
          this.prefix ? this.genAffix('prefix') : null,
          this.genChildrenContent(),
          this.suffix ? this.genAffix('suffix') : null
        ]
      )
    },
    genAffix(type: 'prefix' | 'suffix') {
      return this.$createElement(
        'div',
        {
          class: `group-card__${type}`,
          ref: type
        },
        this[type]
      )
    },
    setLabelWidth() {
      if (!this.outlined || !this.$refs.label) return

      this.labelWidth = this.$refs.label.offsetWidth * 0.75 + 6
    },
    setPrefixWidth() {
      if (!this.$refs.prefix) return

      this.prefixWidth = this.$refs.prefix.offsetWidth
    },
    setPrependWidth() {
      if (!this.outlined || !this.$refs['prepend-inner']) return

      this.prependWidth = this.$refs['prepend-inner'].offsetWidth
    }
  }
})
