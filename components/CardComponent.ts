// Styles
import './CardComponent.sass'

// Components
import VIcon from 'vuetify/src/components/VIcon'
import VLabel from 'vuetify/src/components/VLabel'
import VMessages from 'vuetify/src/components/VMessages'

// Mixins
import Validatable from 'vuetify/src/mixins/validatable'
import Colorable from 'vuetify/src/mixins/colorable'
import Elevatable from 'vuetify/src/mixins/elevatable'
import Measurable from 'vuetify/src/mixins/measurable'
import Themeable from 'vuetify/src/mixins/themeable'

// Utilities
import { convertToUnit, kebabCase } from 'vuetify/src/util/helpers'

// Types
import { VNode, VNodeData, PropType } from 'vue'
import mixins from 'vuetify/src/util/mixins'

const baseMixins = mixins(
  Colorable,
  Elevatable,
  Measurable,
  Themeable,
  Validatable
)

interface options extends InstanceType<typeof baseMixins> {}

/* @vue/component */
export default baseMixins.extend<options>().extend({
  name: 'card-component',

  inheritAttrs: false,

  props: {
    appendIcon: String,
    backgroundColor: {
      type: String,
      default: ''
    },
    // color: {
    //   type: String,
    //   default: 'primary'
    // },
    height: [Number, String],
    hideDetails: Boolean,
    hint: String,
    id: String,
    label: String,
    loading: Boolean,
    persistentHint: Boolean,
    prependIcon: String
  },

  data() {
    return {
      hasMouseDown: false
    }
  },

  computed: {
    classes(): object {
      return {
        'card-component--has-state': this.hasState,
        'card-component--hide-details': this.hideDetails,
        'card-component--is-label-active': this.isLabelActive,
        'card-component--is-dirty': this.isDirty,
        'card-component--is-disabled': this.disabled,
        'card-component--is-focused': this.isFocused,
        'card-component--is-loading':
          this.loading !== false && this.loading !== undefined,
        'card-component--is-readonly': this.readonly,
        ...this.themeClasses,
        ...this.elevationClasses
      }
    },
    computedId(): string {
      return this.id || `content-${this._uid}`
    },
    hasHint(): boolean {
      return (
        !this.hasMessages &&
        !!this.hint &&
        (this.persistentHint || this.isFocused)
      )
    },
    hasLabel(): boolean {
      return !!(this.$slots.label || this.label)
    },
    isDisabled(): boolean {
      return this.disabled || this.readonly
    },
    isLabelActive(): boolean {
      return true
    },
    styles(): object {
      return this.measurableStyles
    }
  },

  methods: {
    genContent() {
      return [this.genPrependSlot(), this.genControl(), this.genAppendSlot()]
    },
    genControl() {
      return this.$createElement(
        'div',
        {
          staticClass: 'card-component__control'
        },
        [this.genContentSlot(), this.genMessages()]
      )
    },
    genDefaultSlot() {
      return [this.genLabel(), this.$slots.default]
    },
    genIcon(type: string, cb?: (e: Event) => void) {
      const icon = (this as any)[`${type}Icon`]
      const eventName = `click:${kebabCase(type)}`

      const data: VNodeData = {
        props: {
          dark: this.dark,
          disabled: this.disabled,
          light: this.light
        },
        on: !(this.$listeners[eventName] || cb)
          ? undefined
          : {
              click: (e: Event) => {
                e.preventDefault()
                e.stopPropagation()

                this.$emit(eventName, e)
                cb && cb(e)
              },
              // Container has g event that will
              // trigger menu open if enclosed
              mouseup: (e: Event) => {
                e.preventDefault()
                e.stopPropagation()
              }
            }
      }

      return this.$createElement(
        'div',
        {
          staticClass: `card-component__icon card-component__icon--${kebabCase(
            type
          )}`,
          key: type + icon
        },
        [this.$createElement(VIcon, data, icon)]
      )
    },
    genContentSlot() {
      const data = {
        staticClass: 'card-component__slot',
        style: { height: convertToUnit(this.height) },
        on: {
          click: this.onClick,
          mousedown: this.onMouseDown,
          mouseup: this.onMouseUp
        },
        ref: 'content-slot'
      }
      this.setTextColor(this.color, data)
      this.setBackgroundColor(this.backgroundColor, data)
      return this.$createElement('div', data, [this.genDefaultSlot()])
    },
    genLabel() {
      if (!this.hasLabel) return null

      const data = {
        props: {
          color: this.backgroundColor,
          dark: this.dark,
          focused: this.hasState,
          for: this.computedId,
          light: this.light
        }
      }
      this.setTextColor(this.backgroundColor, data)
      return this.$createElement(VLabel, data, this.$slots.label || this.label)
    },
    genMessages() {
      if (this.hideDetails) return null

      const messages = this.hasHint ? [this.hint] : this.validations
      const data = {
        props: {
          dark: this.dark,
          light: this.light,
          value: this.hasMessages || this.hasHint ? messages : []
        }
      }
      this.setTextColor(this.backgroundColor, data)

      return this.$createElement(VMessages, data)
    },
    genSlot(type: string, location: string, slot: (VNode | VNode[])[]) {
      if (!slot.length) return null

      const ref = `${type}-${location}`

      return this.$createElement(
        'div',
        {
          staticClass: `card-component__${ref}`,
          ref
        },
        slot
      )
    },
    genPrependSlot() {
      const slot = []

      if (this.$slots.prepend) {
        slot.push(this.$slots.prepend)
      } else if (this.prependIcon) {
        slot.push(this.genIcon('prepend'))
      }

      return this.genSlot('prepend', 'outer', slot)
    },
    genAppendSlot() {
      const slot = []

      // Append icon for text field was really
      // an appended inner icon, v-text-field
      // will overwrite this method in order to obtain
      // backwards compat
      if (this.$slots.append) {
        slot.push(this.$slots.append)
      } else if (this.appendIcon) {
        slot.push(this.genIcon('append'))
      }

      return this.genSlot('append', 'outer', slot)
    },
    onClick(e: Event) {
      this.$emit('click', e)
    },
    onMouseDown(e: Event) {
      this.hasMouseDown = true
      this.$emit('mousedown', e)
    },
    onMouseUp(e: Event) {
      this.hasMouseDown = false
      this.$emit('mouseup', e)
    }
  },
  render(h): VNode {
    const data: VNodeData = {
      staticClass: 'card-component',
      class: this.classes
    }
    return h('div', data, this.genContent())
  }
})
