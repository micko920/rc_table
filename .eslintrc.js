module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: [
    'stylelint',
    'prettier/vue',
    'eslint:recommended',
    'plugin:prettier/recommended',
    'plugin:vue/essential',
    '@nuxtjs',
    'plugin:nuxt/recommended'
  ],
  plugins: ['nuxt', 'prettier', 'vue'],
  // add your custom rules here
  rules: {
    'vue/component-name-in-template-casing': ['error', 'PascalCase'],
    'space-before-function-paren': 'off',
    'node/no-unsupported-features/es-syntax': 'off',
    'vue/max-attributes-per-line': [
      2,
      {
        singleline: 20,
        multiline: {
          max: 1,
          allowFirstLine: false
        }
      }
    ],
    'vue/html-closing-bracket-newline': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/html-self-closing': [
      'error',
      {
        html: {
          void: 'always',
          normal: 'always',
          component: 'always'
        },
        svg: 'always',
        math: 'always'
      }
    ],
    // 'vue/v-bind-style': ['error', 'shorthand'],
    //'vue/component-name-in-template-casing': ['error', 'kebab-case']
    // 'vue/no-spaces-around-equal-signs-in-attribute': 'error',
    'vue/singleline-html-element-content-newline': 'off'
    // 'vue/order-in-components':
    // 'vue/multiline-html-element-content-newline':
    // 'vue/html-closing-bracket-newline':
    // 'vue/html-closing-bracket-spacing':
    // 'vue/prop-name-casing':
    // 'vue/script-indent':
  },
  globals: {
    $nuxt: true
  }
}
